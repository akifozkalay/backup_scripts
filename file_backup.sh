#!/bin/bash

backup_parent_dir="/root/yedekleme"


backup_date=webserver11_web_`date +%Y_%m_%d_%H`
backup_dir="${backup_parent_dir}/${backup_date}"
mkdir -p "${backup_dir}"
chmod 700 "${backup_dir}"


domain_list='/home/'



for domain in /home/*; do

	tar czvf  ${backup_dir}/"$(basename "$domain")".tar.gz /home/"$(basename "$domain")"
	gdcp upload -p 0ACE15ffznnOLUk9PVA "${backup_dir}/"
	rm -rf ${backup_dir}/"$(basename "$domain")".tar.gz
done